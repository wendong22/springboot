/*
Navicat MySQL Data Transfer

Source Server         : Scorpio
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : springboot_03

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2021-07-25 15:49:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `uId` int(20) NOT NULL AUTO_INCREMENT,
  `uName` varchar(255) NOT NULL,
  `uPassword` varchar(255) NOT NULL,
  `uAge` int(20) NOT NULL DEFAULT '0',
  `uBirth` date NOT NULL,
  `uSex` int(20) NOT NULL,
  `tId` int(20) NOT NULL,
  PRIMARY KEY (`uId`),
  KEY `tId` (`tId`),
  CONSTRAINT `tId` FOREIGN KEY (`tId`) REFERENCES `teacher` (`tId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `tId` int(20) NOT NULL AUTO_INCREMENT,
  `tName` varchar(255) NOT NULL,
  `tPassword` varchar(255) NOT NULL,
  `tAge` int(20) NOT NULL,
  `tSex` int(20) NOT NULL,
  `tBirth` date NOT NULL,
  PRIMARY KEY (`tId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
