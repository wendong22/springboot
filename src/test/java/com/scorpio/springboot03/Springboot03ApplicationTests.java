package com.scorpio.springboot03;

import com.scorpio.mapper.StudentMapper;
import com.scorpio.pojo.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
class Springboot03ApplicationTests {
    @Autowired
    private StudentMapper studentMapper;
    @Test
    void contextLoads() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name","张三");
        map.put("password","111111");
        Student student = studentMapper.selectByNameAndPassword(map);
        System.out.println(student);


    }

}
