package com.scorpio.service;

import com.scorpio.pojo.Admin;
import java.util.List;
import java.util.Map;


public interface AdminService {
    Admin login(Map map);
    List<Admin> selectAll();
    List<Admin> selectByName(String adminName);
    Admin selectById(Integer id);
    int addAdmin(Admin admin);
    int deleteById(Integer id);
    int updateById(Admin admin);
}
