package com.scorpio.service;

import com.scorpio.pojo.Teacher;

import java.util.List;
import java.util.Map;

public interface TeacherService {
    Teacher selectByNameAndPassword(Map map);
    List<Teacher> selectAll();
    List<Teacher> selectByName(String tName);
    Teacher selectById(Integer id);
    int updateById(Teacher teacher);
    int updateAgeById(Integer id);
    int deleteById(Integer id);
    int addTeacher(Teacher teacher);
}
