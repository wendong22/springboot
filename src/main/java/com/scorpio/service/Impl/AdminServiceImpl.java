package com.scorpio.service.Impl;

import com.scorpio.mapper.AdminMapper;
import com.scorpio.pojo.Admin;
import com.scorpio.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin login(Map map) {
        return adminMapper.selectByNameAndPassword(map);
    }

    @Override
    public List<Admin> selectAll() {
        return adminMapper.selectAll();
    }

    @Override
    public List<Admin> selectByName(String adminName) {
        return adminMapper.selectByName(adminName);
    }

    @Override
    public Admin selectById(Integer id) {
        return adminMapper.selectById(id);
    }

    @Override
    public int addAdmin(Admin admin) {
        return adminMapper.addAdmin(admin);
    }

    @Override
    public int deleteById(Integer id) {
        return adminMapper.deleteById(id);
    }

    @Override
    public int updateById(Admin admin) {
        return adminMapper.updateById(admin);
    }
}
