package com.scorpio.service.Impl;

import com.scorpio.mapper.StudentMapper;
import com.scorpio.pojo.Student;
import com.scorpio.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Student selectByNameAndPassword(Map map) {
        return studentMapper.selectByNameAndPassword(map);
    }

    @Override
    public List<Student> selectAll() {
        return studentMapper.selectAll();
    }

    @Override
    public List<Student> selectByName(String uName) {
        return studentMapper.selectByName(uName);
    }

    @Override
    public Student selectById(Integer id) {
        return studentMapper.selectById(id);
    }

    @Override
    public int updateById(Student student) {
        return studentMapper.updateById(student);
    }

    @Override
    public int updateAgeById(Integer id) {
        return studentMapper.updateAgeById(id);
    }

    @Override
    public int deleteById(Integer id) {
        return studentMapper.deleteById(id);
    }

    @Override
    public int addStudent(Student student) { return studentMapper.addStudent(student); }
}
