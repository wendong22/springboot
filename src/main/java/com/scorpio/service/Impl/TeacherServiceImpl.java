package com.scorpio.service.Impl;

import com.scorpio.mapper.TeacherMapper;
import com.scorpio.pojo.Teacher;
import com.scorpio.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public Teacher selectByNameAndPassword(Map map) {
        return teacherMapper.selectByNameAndPassword(map);
    }

    @Override
    public List<Teacher> selectAll() {
        return teacherMapper.selectAll();
    }

    @Override
    public List<Teacher> selectByName(String tName) {
        return teacherMapper.selectByName(tName);
    }

    @Override
    public Teacher selectById(Integer tId) {
        return teacherMapper.selectById(tId);
    }

    @Override
    public int updateById(Teacher teacher) {
        return teacherMapper.updateById(teacher);
    }

    @Override
    public int updateAgeById(Integer id) {
        return teacherMapper.updateAgeById(id);
    }

    @Override
    public int deleteById(Integer id) {
        return teacherMapper.deleteById(id);
    }

    @Override
    public int addTeacher(Teacher teacher) {
        return teacherMapper.addTeacher(teacher);
    }
}
