package com.scorpio.service;

import com.scorpio.pojo.Student;

import java.util.List;
import java.util.Map;

public interface StudentService {
    Student selectByNameAndPassword(Map map);
    List<Student> selectAll();
    List<Student> selectByName(String uName);
    Student selectById(Integer id);
    int updateById(Student student);
    int updateAgeById(Integer id);
    int deleteById(Integer id);
    int addStudent(Student student);
}
