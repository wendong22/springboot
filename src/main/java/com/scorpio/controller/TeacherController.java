package com.scorpio.controller;

import com.scorpio.pojo.Student;
import com.scorpio.pojo.Teacher;
import com.scorpio.service.StudentService;
import com.scorpio.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private StudentService studentService;

    @RequestMapping("/getAll")
    public String getAll(Model model){
        List<Teacher> teacherList = teacherService.selectAll();
        model.addAttribute("teachers",teacherList);
        return "teacher/teacher-list";
    }

    @RequestMapping("/select")
    public String selectByName(String teacherName,Model model){
        List<Teacher> teacherList = null;
        teacherList = teacherService.selectByName(teacherName);
        if (teacherList.isEmpty()){
            model.addAttribute("msg","未查到该用户");
            teacherList = teacherService.selectAll();
        }
        model.addAttribute("teachers",teacherList);
        return "teacher/teacher-list";
    }

    @RequestMapping("/toUpdate/{id}")
    public String toUpdate(@PathVariable Integer id, Model model){
        Teacher teacher = teacherService.selectById(id);
        model.addAttribute("teacher",teacher);
        return "teacher/teacher-update";
    }

    @RequestMapping("/verify")
    @ResponseBody
    public String verify(Integer id,String oldPassword){
        String msg = "";
        Teacher teacher = teacherService.selectById(id);
        if (oldPassword.equals(teacher.getTPassword())){
            msg = "OK";
        }else {
            msg = "密码错误";
        }
        return msg;
    }

    @PostMapping("/update")
    public String update(Teacher teacher,RedirectAttributes redirectAttributes){
        int i = teacherService.updateById(teacher);
        int i1 = teacherService.updateAgeById(teacher.getTId());
        if (i>0 && i1>0){
            redirectAttributes.addFlashAttribute("msg","修改成功");
        }else {
            redirectAttributes.addFlashAttribute("msg","修改失败");
        }
        return "redirect:/teacher/getAll";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable Integer id, RedirectAttributes redirectAttributes){
        int i = teacherService.deleteById(id);
        if (i>0){
            redirectAttributes.addFlashAttribute("msg","删除成功");
        }else {
            redirectAttributes.addFlashAttribute("msg","删除失败");
        }
        return "redirect:/teacher/getAll";
    }

    @GetMapping("/add")
    public String toAdd(){
        return "teacher/teacher-add";
    }

    @PostMapping("/add")
    public String add(Teacher teacher){
        int i = teacherService.addTeacher(teacher);
        return "redirect:/teacher/getAll";
    }
}
