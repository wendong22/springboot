package com.scorpio.controller;

import com.scorpio.pojo.Admin;
import com.scorpio.pojo.Student;
import com.scorpio.pojo.Teacher;
import com.scorpio.service.AdminService;
import com.scorpio.service.StudentService;
import com.scorpio.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Controller
public class LoginController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private TeacherService teacherService;

    @RequestMapping("/welcome")
    public String welcome(){
        return "welcome";
    }
    @PostMapping("/login")
    public String login(Integer role, String name, String password, Model model, HttpSession session){
        HashMap<String, Object> map = new HashMap<>();
        map.put("name",name);
        map.put("password",password);
        switch (role){
            case 1 :
                Admin loginAdmin = adminService.login(map);
                if (loginAdmin!=null){
                    session.setAttribute("loginName",name);
                    session.setAttribute("role",role);
                    return "redirect:/admin-main.html";
                }else {
                    model.addAttribute("msg","用户名或密码错误");
                    return "index";
                }
            case 2 :
                Student loginStudent = studentService.selectByNameAndPassword(map);
                if (loginStudent!=null){
                    session.setAttribute("loginName",name);
                    session.setAttribute("role",role);
                    session.setAttribute("uID",loginStudent.getUId());
                    return "redirect:/student-main.html";
                }else {
                    model.addAttribute("msg","用户名或密码错误！");
                    return "index";
                }
            case 3:
                Teacher loginTeacher = teacherService.selectByNameAndPassword(map);
                if (loginTeacher!=null){
                    session.setAttribute("loginName",name);
                    session.setAttribute("role",role);
                    session.setAttribute("tID",loginTeacher.getTId());
                    return "redirect:/teacher-main.html";
                }else {
                    model.addAttribute("msg","用户名或密码错误！");
                    return "index";
                }
            default:{
                model.addAttribute("msg","角色错误！");
                return "index";
            }
        }
    }

    @RequestMapping("/loginOut")
    public String loginOut(HttpSession session){
        session.invalidate();
        return "index";
    }
}
