package com.scorpio.controller;

import com.scorpio.pojo.Student;
import com.scorpio.pojo.Teacher;
import com.scorpio.service.StudentService;
import com.scorpio.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private TeacherService teacherService;

    @RequestMapping("/getAll")
    public String getAll(Model model){
        List<Student> studentList = studentService.selectAll();
        model.addAttribute("students",studentList);
        return "student/student-list";
    }

    @RequestMapping("/select")
    public String selectByName(String studentName,Model model){
        List<Student> studentList = null;
        studentList = studentService.selectByName(studentName);
        if (studentList.isEmpty()){
            model.addAttribute("msg","未查到该用户");
            studentList = studentService.selectAll();
        }
        model.addAttribute("students",studentList);
        return "student/student-list";
    }

    @RequestMapping("/toUpdate/{id}")
    public String toUpdate(@PathVariable Integer id, Model model){
        Student student = studentService.selectById(id);
        model.addAttribute("student",student);
        return "student/student-update";
    }

    @PostMapping("/update")
    public String update(Student student, Model model,RedirectAttributes redirectAttributes){
        int i = studentService.updateById(student);
        int i1 = studentService.updateAgeById(student.getUId());
        if (i>0 && i1>0){
            redirectAttributes.addFlashAttribute("msg","修改成功");
        }else {
            redirectAttributes.addFlashAttribute("msg","修改失败");
        }
        return "redirect:/student/getAll";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable Integer id, RedirectAttributes redirectAttributes){
        int i = studentService.deleteById(id);
        if (i>0){
            redirectAttributes.addFlashAttribute("msg","删除成功");
        }else {
            redirectAttributes.addFlashAttribute("msg","删除失败");
        }
        return "redirect:/student/getAll";
    }

    @GetMapping("/add")
    public String toAdd(Model model){
        List<Teacher> teacherList = teacherService.selectAll();
        model.addAttribute("teacherList",teacherList);
        return "student/student-add";
    }

    @PostMapping("/add")
    public String add(Student student){
        student.setTeacher(teacherService.selectById(student.getTeacher().getTId()));
        int i = studentService.addStudent(student);
        return "redirect:/student/getAll";
    }

    @RequestMapping("/verify")
    @ResponseBody
    public String verify(Integer id,String oldPassword){
        String msg = "";
        Student student = studentService.selectById(id);
        if (oldPassword.equals(student.getUPassword())){
            msg = "OK";
        }else {
            msg = "密码错误";
        }
        return msg;
    }
}
