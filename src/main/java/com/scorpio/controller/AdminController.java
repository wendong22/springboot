package com.scorpio.controller;

import com.scorpio.pojo.Admin;
import com.scorpio.pojo.Student;
import com.scorpio.service.AdminService;
import com.scorpio.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private StudentService studentService;

    @RequestMapping("/getAll")
    public String getAll(Model model){
        //获取admin列表
        List<Admin> adminList = adminService.selectAll();
        model.addAttribute("admins",adminList);
        return "admin/admin-list";
    }

    @RequestMapping("/select")
    public String selectByName(String adminName,Model model){
        List<Admin> adminList = null;
        adminList = adminService.selectByName(adminName);
        for (Admin admin : adminList) {
            System.out.println(admin);
        }
        if (adminList.isEmpty()){
            model.addAttribute("msg","未查到该用户");
            adminList = adminService.selectAll();
        }
        model.addAttribute("admins",adminList);
        return "admin/admin-list";
    }


    @RequestMapping("/toAdd")
    public String toAdd(){
        return "admin/admin-add";
    }

    @RequestMapping("/add")
    public String add(Admin admin,RedirectAttributes redirectAttributes){
        int i = adminService.addAdmin(admin);
        if (i>0){
            redirectAttributes.addFlashAttribute("msg","添加成功");

        }else {
            redirectAttributes.addFlashAttribute("msg","添加失败");
        }
        return "redirect:/admin/getAll";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable Integer id,RedirectAttributes redirectAttributes){
        System.out.println(id);
        int i = adminService.deleteById(id);
        if (i>0){
            redirectAttributes.addFlashAttribute("msg","删除成功");

        }else {
            redirectAttributes.addFlashAttribute("msg","删除失败");
        }
        return "redirect:/admin/getAll";
    }

    @RequestMapping("/toUpdate/{id}")
    public String toUpdate(@PathVariable Integer id,Model model){
        Admin admin = adminService.selectById(id);
        model.addAttribute("admin",admin);
        return "admin/admin-update";
    }

    @RequestMapping("/update")
    public String update(Admin admin){
        int i = adminService.updateById(admin);
        return "redirect:/admin/getAll";
    }

    @RequestMapping("/verify")
    @ResponseBody
    public String verify(Integer id,String oldPassword){
        String msg = "";
        Admin admin = adminService.selectById(id);
        if (oldPassword.equals(admin.getPassword())){
            msg = "OK";
        }else {
            msg = "密码错误";
        }
        return msg;
    }











}
