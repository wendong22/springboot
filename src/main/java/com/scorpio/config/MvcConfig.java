package com.scorpio.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//扩展MVC的配置
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
        registry.addViewController("/admin-main.html").setViewName("admin/admin-main");
        registry.addViewController("/student-main.html").setViewName("student/student-main");
        registry.addViewController("/teacher-main.html").setViewName("teacher/teacher-main");
    }
}
