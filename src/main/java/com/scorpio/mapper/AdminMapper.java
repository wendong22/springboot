package com.scorpio.mapper;
import com.scorpio.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AdminMapper {
    Admin selectByNameAndPassword(Map map);
    List<Admin> selectAll();
    List<Admin> selectByName(String adminName);
    Admin selectById(Integer id);
    int addAdmin(Admin admin);
    int deleteById(Integer id);
    int updateById(Admin admin);

}
