package com.scorpio.mapper;

import com.scorpio.pojo.Admin;
import com.scorpio.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface StudentMapper {
    Student selectByNameAndPassword(Map map);
    List<Student> selectAll();
    List<Student> selectByName(String uName);
    Student selectById(Integer id);
    int updateById(Student student);
    int updateAgeById(Integer id);
    int deleteById(Integer id);
    int addStudent(Student student);

}
