package com.scorpio.mapper;

import com.scorpio.pojo.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TeacherMapper {
    Teacher selectByNameAndPassword(Map map);
    List<Teacher> selectAll();
    List<Teacher> selectByName(String tName);
    Teacher selectById(Integer id);
    int updateById(Teacher teacher);
    int updateAgeById(Integer id);
    int deleteById(Integer id);
    int addTeacher(Teacher teacher);

}
