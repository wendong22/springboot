package com.scorpio.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Student {
    private Integer uId;
    private String uName;
    private String uPassword;
    private Integer uAge;
    private Integer uSex;
    private Date uBirth;
    private Teacher teacher;

}
