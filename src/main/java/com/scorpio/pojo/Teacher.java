package com.scorpio.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    private Integer tId;
    private String tName;
    private String tPassword;
    private Integer tAge;
    private Integer tSex;
    private Date tBirth;

}
